const nestedArray = [1, [2], [[3]], [[[4]]]]; 

function flatten(element, ret = []) {
    for (const entry of element) {
        if (Array.isArray(entry)){
            flatten(entry, ret);
        } else {
            ret.push(entry);
        }
    }
    return ret;
}
console.log(flatten(nestedArray));